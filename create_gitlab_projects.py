#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 29 14:32:43 2019

@author: Jacek Pawlyta

Creating projects and deadlines in GitLab for group of students

"""

# request lib allows us to communicate with GitLab API via http
import requests
# sys lib allows us to capture system errors
import sys

# Parameters
PROJECT_NAME = "zadanie_1_gr_1.2"
PROJECT_MEMBERSHIP_EXPIRATION_DATE = "2019-10-16"
NICKNAMES_FILENAME = "gr1_2_nicknames"
#NICKNAMES_FILENAME = "gr2_2_nicknames"

# GitLab login info
import mytoken
PERSONAL_ACCESS_TOKEN_MATLAB = mytoken.TOKEN

# GitLab group
GROUP_ID = "6162269"
PROJECT_ACCESS_LEVEL = "30"

# defining exceptions
class ProjectCreationException(Exception):
    def __init__(self, message, user, task_name):
        self.message = message
        self.user = user
        self.task_name = task_name

    def __str__(self):
        return repr(self.task_name + " " + self.user + "::" + self.message)


class UserIdRequestException(Exception):
    def __init__(self, message, user):
        self.message = message
        self.user = user

    def __str__(self):
        return repr(self.user + "::" + self.message)


class UserProjectMemebershipException(Exception):
    def __init__(self, message, user):
        self.message = message
        self.user = user

    def __str__(self):
        return repr(self.user + "::" + self.message)


class UserProjectUnblockException(Exception):
    def __init__(self, message, user):
        self.message = message
        self.user = user

    def __str__(self):
        return repr(self.user + "::" + self.message)


task_name = PROJECT_NAME

# reading nicknames from the file
try:
    NicknamesFile = open(NICKNAMES_FILENAME)
except IOError:
    print("can not open file ")
    sys.exit()


# initialising nicknameTable list
nickNamesTable=[]

for nickNames in NicknamesFile:
    nickNames = nickNames.replace("\n","")
    nickNamesTable.append(nickNames)

# Creating GitLab public projects containing readme files and deadlines for each nickname in nickNameTable

for user in nickNamesTable:
    try:
        userIdRequestResponse = requests.get(
            'https://gitlab.com/api/v4/users',
            params={'username': user},
            headers={'Private-Token': PERSONAL_ACCESS_TOKEN_MATLAB},
        )
        if not userIdRequestResponse:
            raise UserIdRequestException(str(userIdRequestResponse.content), user)

        if len(userIdRequestResponse.content) == 2:
            raise UserIdRequestException("user not found: ", user)

        userId = userIdRequestResponse.json()[0]["id"]

        project_name = task_name + "_" + user

        projectCreationResponse = requests.post(
            'https://gitlab.com/api/v4/projects',
            data={'name': project_name,
                  'namespace_id': GROUP_ID,
                  'initialize_with_readme': 'true',
                  'repository_access_level': 'enabled',
                  'visibility': 'public'
                  },
            headers={'Private-Token': PERSONAL_ACCESS_TOKEN_MATLAB},
        )

        if not projectCreationResponse:
            raise ProjectCreationException(str(projectCreationResponse.content), user, task_name)

        projectId = projectCreationResponse.json()["id"]

        userProjectAssignmentUrl = "https://gitlab.com/api/v4/projects/" + str(projectId) + "/members"

        userProjectAssigmentResponse = requests.post(
            userProjectAssignmentUrl,
            data={'user_id': userId,
                  'access_level': PROJECT_ACCESS_LEVEL,
                  'expires_at': PROJECT_MEMBERSHIP_EXPIRATION_DATE
                  },
            headers={'Private-Token': PERSONAL_ACCESS_TOKEN_MATLAB},
        )
        if not userProjectAssigmentResponse:
            raise UserProjectMemebershipException(str(userProjectAssigmentResponse.content), user)

        userProjectUnblockUrl = "https://gitlab.com/api/v4/projects/" + str(projectId) + "/protected_branches/master"
        userProjectUnblockResponse = requests.delete(
            userProjectUnblockUrl,
            headers={'Private-Token': PERSONAL_ACCESS_TOKEN_MATLAB},
        )
        if not userProjectUnblockResponse:
            raise UserProjectUnblockException(str(userProjectUnblockResponse.content), user)
## EXCEPTIONS
    except ProjectCreationException as error:
        print(str(error))
    except UserIdRequestException as error:
        print(str(error))
    except UserProjectMemebershipException as error:
        print(str(error))
    except UserProjectUnblockException as error:
        print(str(error))
